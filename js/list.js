jQuery(document).ready(function ($) {
    var options = {
        valueNames: [
            'entity_type',
            'bundle',
            'field',
            'column',
            'type'
        ]
    };

    var columnList = new List('column-list', options);
    const $checkboxes = $('#column-list input[type=checkbox]');

    $(options.valueNames.map(v => `#search-${v}`).join(', ')).on('keyup', function (event) {
        columnList.filter(function (item) {
          const values = item.values();
          return options.valueNames.reduce(function (pass, value) {
             if (!document.getElementById(`search-${value}`)) return pass;
             const searchTerm = $(`#search-${value}`).val();
             const itemContent = values[value].toLowerCase();
             if (!itemContent.includes(searchTerm.toLowerCase())) return false;
             return pass;
          }, true);
        });
    });



    $checkboxes.on('change', function () {
        let val = [];
        $checkboxes.each(function (i, checkbox) {
            if (checkbox.checked) {
                val.push(checkbox.name);
            }
        });
        $('#selected').val(val.join('|'));
    })

    $('#select_visible').on('click', function (event) {
        event.preventDefault();
        columnList.visibleItems.forEach(function (item) {
            item.elm.querySelector('input').checked = true;
        });
        $checkboxes.trigger('change');
    });

    $('#unselect_visible').on('click', function (event) {
        event.preventDefault();
        columnList.visibleItems.forEach(function (item) {
            item.elm.querySelector('input').checked = false;
        });
        $checkboxes.trigger('change');
    });

})
