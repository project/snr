<?php

namespace Drupal\snr_safe\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\snr_safe\SNRSafeController;
use Drupal\snr\SNROperation;
use Drupal\snr\SNRTarget;
use Drupal\snr_safe\SNRSafeTarget;

class SNRSafeForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'snr_safe_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['search_text'] = [
      '#type' => 'textfield',
      '#title' => 'Search Text'
    ];

    $form['replace_text'] = [
      '#type' => 'textfield',
      '#title' => 'Replace Text'
    ];

    $form['tables'] = [
      '#type' => 'container',
      '#attached' => [
        'library' => [
          'snr/list'
        ]
      ]
    ];

    $entityFields = SNRSafeController::entityFields();

    ob_start();

    echo "
    <div id='column-list'>
        <table>
            <thead>
                <tr>
                    <th><button id='select_visible'>Select</button></th>
                    <th>Entity Type</th>
                    <th>Bundle</th>
                    <th>Field</th>
                </tr>
                <tr>
                    <th><button id='unselect_visible'>Unselect</button></th>
                    <th><input type='text' id='search-entity_type' placeholder='Filter' /></th>
                    <th><input type='text' id='search-bundle' placeholder='Filter' /></th>
                    <th><input type='text' id='search-field' placeholder='Filter' /></th>
                </tr>
            </thead>
            <tbody class='list'>";

    /** @var \Drupal\snr\SNREntityFieldMap $entityFieldMap */
    foreach ($entityFields as $entity_type_id => $entityFieldMap) {
      $entity_bundle_info = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type_id);
      $entity_type_label = \Drupal::service('entity_type.manager')->getDefinition($entity_type_id)->getLabel();

      /** @var \Drupal\snr\SNRBundleFieldMap $bundleFieldMap */
      foreach ($entityFieldMap->bundles as $bundle_key => $bundleFieldMap) {
        $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type_id, $bundle_key);

        /** @var \Drupal\snr\SNRFieldMap $fieldMap */
        foreach ($bundleFieldMap->fields as $slug => $fieldMap) {
          if (!isset($fieldMap->columns)) continue;
          $definition = $fields[$slug];

          foreach ($fieldMap->columns as $column) {
            $label = $definition->getConfig($bundle_key)->getLabel();
            echo "<tr>
               <td><input type='checkbox' name='{$entity_type_id}++{$bundle_key}++{$slug}++{$column}' /></td>
               <td class='entity_type'>{$entity_type_label}</td>
               <td class='bundle'>{$entity_bundle_info[$bundle_key]['label']}</td>
               <td class='field'>{$label}</td>
            </tr>";
          }
        }
      }
    }

    echo "</tbody></table></div>";

    $form['table'] = [
      '#markup' => ob_get_clean(),
      '#allowed_tags' => ['input', 'div', 'table', 'tr', 'th', 'td', 'thead', 'tbody', 'button'],
      '#weight' => 999
    ];

    $form['selected'] = [
      '#type' => 'hidden',
      '#default_value' => '',
      '#attributes' => ['id' => 'selected']
    ];


    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 998,
      'search' => [
        '#type' => 'submit',
        '#value' => 'Search',
        '#name' => 'search'
      ],
      'replace' => [
        '#type' => 'submit',
        '#value' => 'Replace',
        '#name' => 'replace'
      ]
    ];



    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $selected = array_unique(explode('|', $form_state->getValue('selected')));
    $search = $form_state->getValue('search_text');
    $replace = $form_state->getValue('replace_text');
    $entityFields = SNRSafeController::entityFields();
    $operation = new SNROperation($search, $replace); // TODO options
    $batch = [
      'operations' => [],
    ];


    $button = $form_state->getTriggeringElement();
    if ($button['#name'] === 'search') {
      $batch['title'] = t('Searching for @search', ['@search' => $search]);
      $batch['finished'] = ['\Drupal\snr_safe\SNRSafeController', 'searchBatchFinished'];
      $batchOperation = 'searchBatchOperation';
    } else if ($button['#name'] === 'replace') {
      $batch['title'] = t('Replacing @search with @replace', ['@search' => $search, '@replace' => $replace]);
      $batch['finished'] = ['\Drupal\snr_safe\SNRSafeController', 'replaceBatchFinished'];
      $batchOperation = 'replaceBatchOperation';
    }

    $table_column_map = [];

    // Columns can be shared across bundles, but we want to expose bundles for filtering
    foreach ($selected as $column_path) {
      [$entity_type_id, $bundle_key, $field_slug, $column] = explode('++', $column_path);
      $column = str_replace("{$field_slug}_", '', $column);
      $table_column_map[] = "{$entity_type_id}++{$bundle_key}++{$field_slug}++{$column}";
    }

    //    return drupal_set_message(print_r($table_column_map, true));

    $table_column_map = array_unique($table_column_map);

    foreach ($table_column_map as $table_column_triple) {
      [$entity_type_id, $bundle_key, $field_slug, $column] = explode('++', $table_column_triple);
      $batch['operations'][] = [
        ['\Drupal\snr_safe\SNRSafeController', $batchOperation],
        [$operation, new SNRSafeTarget($entity_type_id, $bundle_key, $field_slug, $column)]
      ];
    }

    if ($batchOperation === 'replaceBatchOperation') {
      $batch['operations'] = [[
        [SNRSafeController::class, 'batchProcessor'],
        [$batch['operations']]
      ]];
    }

    fprint($batch['operations'], 'log');



    batch_set($batch);

  }

}
