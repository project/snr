<?php

namespace Drupal\snr_safe;

class SNRSafeTarget {
  public $entity_type;
  public $bundle;
  public $field;
  public $column;

  public function __construct($entity_type, $bundle, $field, $column) {
    $this->entity_type = $entity_type;
    $this->bundle = $bundle;
    $this->field = $field;
    $this->column = $column;
  }

}
