<?php

namespace Drupal\snr_safe;

use Drupal\Core\Render\Markup;
use Drupal\snr\SNRBundleFieldMap;
use Drupal\snr\SNRController;
use Drupal\snr\SNREntityFieldMap;
use Drupal\snr\SNRFieldMap;
use Drupal\snr\SNROperation;
use Drupal\snr\SNRTarget;

class SNRSafeController extends SNRController {

  public static function searchBatchOperation(SNROperation $operation, $target, &$context): void {
    $reults = 0;
    try {
      $results = \Drupal::entityQuery($target->entity_type)
        ->condition('type', $target->bundle)
        ->condition($target->field, $operation->search, 'CONTAINS')
        ->count()
        ->execute();
    } catch (\Throwable $e) {

    }
    if ($results > 0) {
      $context['results'][] = [$target, $results];
    }
  }

  public static function replaceBatchOperation(SNROperation $operation, $target, &$context): void {
//    fprint($context, 'log');
    $ids = 0;
    try {
      $ids = \Drupal::entityQuery(str_replace('"', '', $target->entity_type))
        ->condition('type', $target->bundle)
        ->condition($target->field, $operation->search, 'CONTAINS')
        ->execute();
    } catch (\Throwable $e) {
      \Drupal::logger('snr_safe')->error($e->getMessage());
    }

    if (count($ids) > 0) {
      $context['results'][] = [$target, count($ids)];
    }

    $operations = [];
    foreach ($ids as $vid => $id) {
      $operations[] = [
        [static::class, 'replaceEntity'],
        [$operation, $target, $id]
      ];
    }
    static::addOperationsToBatchContext($operations, $context);
  }

  public static function batchProcessor($initial_operations, &$context) {

    if (empty($context['sandbox'])) {
      $context['sandbox'] = [
        'progress' => 0,
        'max' => count($initial_operations),
        'operations' => $initial_operations
      ];
    }

    $size = 5;
    $processed = 0;

    foreach ($context['sandbox']['operations'] as $index => $operation) {
      $func = $operation[0];
      $params = $operation[1];
      $params[] = &$context;
      call_user_func_array($func, $params);
//      $func(...[...$params, $context]); // Tacks the content (by reference) onto the parameters and calls the function
      $context['sandbox']['progress']++;
      $context['message'] .= ' | Completed ' . $context['sandbox']['progress'] . ' of ' . $context['sandbox']['max'];
      unset($context['sandbox']['operations'][$index]);
      $processed++;
      if ($processed === $size) {
        break;
      }
    }

    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  public static function addOperationsToBatchContext($operations, &$context) {
    foreach ($operations as $operation) {
      $context['sandbox']['operations'][] = $operation;
      $context['sandbox']['max']++;
    }
  }

  public static function replaceEntity(SNROperation $operation, $target, $entity_id, &$context): void {

    /** @var \Drupal\Core\Entity\RevisionableContentEntityBase $entity */
    $entity = \Drupal::entityTypeManager()
      ->getStorage($target->entity_type)
      ->load($entity_id);

    $current = $entity->{$target->field};

    $current->{$target->column} = str_replace($operation->search, $operation->replace, $current->{$target->column});

    $entity->{$target->field} =  $current;

    $entity->setNewRevision(TRUE);

    $entity->save();

    $context['message'] = 'Updated ' . $entity->label();
  }

  public static function searchBatchFinished($success, $results, $operations) {

    $table = [
      '#type' => 'table',
      '#header' => [
        'entity_type' => 'Entity_type',
        'field' => 'Field',
        'found' => 'Found'
      ],
      '#rows' => [],
      '#empty' => 'No results found'
    ];

    foreach ($results as $i => $result) {
      $row = [
        'entity_type' => $result[0]->entity_type,
        'field' => $result[0]->field,
        'found' => $result[1]
      ];
      $table['#rows'][] = $row;
    }

    /** @var \Drupal\Core\Render\Renderer $renderer */
    $renderer = \Drupal::service('renderer');
    $markup = $renderer->render($table);

    \Drupal::messenger()->addMessage($markup);
  }

  /**
   * @return \Drupal\snr\SNREntityFieldMap[]
   */
  public static function entityFields(): array {
    $interesting_types = static::getFieldTypes();
    $column_map = static::getColumnMap();
    $return = [];

    /** @var \Drupal\Core\Entity\EntityTypeManager $manager */
    $manager = \Drupal::service('entity_type.manager');
    $entity_types = $manager->getDefinitions();

    /** @var \Drupal\Core\Entity\EntityTypeInterface $entity_type */
    foreach ($entity_types as $entity_type_id => $entity_type) {
      if (!$entity_type->isRevisionable()) continue; // SNR safe only processes revisionable entities
      $entityFieldMap = new SNREntityFieldMap();
      $storage = $manager->getStorage($entity_type_id);
      if (!method_exists($storage, 'getTableMapping')) continue;
      /** @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage $mapping */
      $mapping = $storage->getTableMapping();
      $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type_id);
      $base_columns = $mapping->getAllColumns($mapping->getDataTable());

      foreach ($bundles as $bundle_key => $bundle) {
        $bundleMap = new SNRBundleFieldMap();
        $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type_id, $bundle_key);

        /** @var \Drupal\Core\Field\FieldDefinitionInterface $def */
        foreach ($field_definitions as $slug => $def) {
          if (strpos($slug, 'revision') !== false) continue;
          $type = $def->getType();
          if (!in_array($type, $interesting_types)) continue;
          $field_storage = $def->getFieldStorageDefinition();

          try {
            $fieldMap = new SNRFieldMap();
            $base_cols = empty($base_columns) ? $mapping->getAllColumns($mapping->getFieldTableName($slug)) : $base_columns;

            $fieldMap->type = $type;
            $fieldMap->table = $mapping->getFieldTableName($slug);
            $fieldMap->columns = array_filter($mapping->getColumnNames($slug), function ($column) use ($slug, $column_map) {
              if (!in_array(
                str_replace("{$slug}_", '', $column), // The subcolumn ('value', 'uri')
                array_merge(['value'], ...array_values($column_map)) // valid columns
              )) return false; // Don't include columns like "format"
              return true;
            });
            if (in_array(current(array_values($fieldMap->columns)), $base_cols)) {
              $fieldMap->revision_table = $mapping->getRevisionDataTable();
            } else {
              $fieldMap->revision_table = $mapping->getDedicatedRevisionTableName($field_storage);
            }

          } catch (\Throwable $e) {

          }
          if ($fieldMap) {
            $bundleMap->fields[$slug] = $fieldMap;
          }

        }
        $entityFieldMap->bundles[$bundle_key] = $bundleMap;
      }
      $return[$entity_type_id] = $entityFieldMap;
    }

    return $return;
  }
}
