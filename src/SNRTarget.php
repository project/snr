<?php

namespace Drupal\snr;

class SNRTarget {
  public $table;
  public $column;

  public function __construct($table, $column) {
    $this->table = $table;
    $this->column = $column;
  }

}