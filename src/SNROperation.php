<?php

namespace Drupal\snr;

class SNROperation {
  public $search;
  public $replace;
  public $whole_word = false;
  public $regex = false;

  public function __construct($search, $replace) {
    $this->search = $search;
    $this->replace = $replace;
  }

}