<?php

namespace Drupal\snr\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\snr\SNRController;
use Drupal\snr\SNROperation;
use Drupal\snr\SNRTarget;

class SNRForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'snr_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['search_text'] = [
      '#type' => 'textfield',
      '#title' => 'Search Text'
    ];

    $form['replace_text'] = [
      '#type' => 'textfield',
      '#title' => 'Replace Text'
    ];

    $form['options'] = [
      '#type' => 'fieldset',
      '#title' => 'Options (not working yet)',
      'case' => [
        '#type' => 'checkbox',
        '#title' => 'Case-Sensitive',
        '#default_value' => TRUE
      ],
      'whole_word' => [
        '#type' => 'checkbox',
        '#title' => 'Whole Word'
      ],
      'regex' => [
        '#type' => 'checkbox',
        '#title' => 'Use Regex'
      ]
    ];

    $form['tables'] = [
      '#type' => 'container',
      '#attached' => [
        'library' => [
          'snr/list'
        ]
      ]
    ];

    $entityFields = SNRController::entityFields();

    ob_start();

    echo "
    <div id='column-list'>
        <table>
            <thead>
                <tr>
                    <th><button id='select_visible'>Select</button></th>
                    <th>Entity Type</th>
                    <th>Bundle</th>
                    <th>Field</th>
                    <th>Column</th>
                    <th>Type</th>
                </tr>
                <tr>
                    <th><button id='unselect_visible'>Unselect</button></th>
                    <th><input type='text' id='search-entity_type' placeholder='Filter' /></th>
                    <th><input type='text' id='search-bundle' placeholder='Filter' /></th>
                    <th><input type='text' id='search-field' placeholder='Filter' /></th>
                    <th><input type='text' id='search-column' placeholder='Filter' /></th>
                    <th><input type='text' id='search-type' placeholder='Filter' /></th>
                </tr>
            </thead>
            <tbody class='list'>";

    /** @var \Drupal\snr\SNREntityFieldMap $entityFieldMap */
    foreach ($entityFields as $entity_type_id => $entityFieldMap) {

      /** @var \Drupal\snr\SNRBundleFieldMap $bundleFieldMap */
      foreach ($entityFieldMap->bundles as $bundle_key => $bundleFieldMap) {

        /** @var \Drupal\snr\SNRFieldMap $fieldMap */
        foreach ($bundleFieldMap->fields as $slug => $fieldMap) {
          if (!isset($fieldMap->columns)) continue;

          foreach ($fieldMap->columns as $column) {
            echo "<tr>
               <td><input type='checkbox' name='{$entity_type_id}++{$bundle_key}++{$slug}++{$column}' /></td>
               <td class='entity_type'>{$entity_type_id}</td>
               <td class='bundle'>{$bundle_key}</td>
               <td class='field'>{$slug}</td>
               <td class='column'>{$column}</td>
               <td class='type'>{$fieldMap->type}</td>
            </tr>";
          }
        }
      }
    }

    echo "</tbody></table></div>";

    $form['table'] = [
      '#markup' => ob_get_clean(),
      '#allowed_tags' => ['input', 'div', 'table', 'tr', 'th', 'td', 'thead', 'tbody', 'button'],
      '#weight' => 999
    ];

    $form['selected'] = [
      '#type' => 'hidden',
      '#default_value' => '',
      '#attributes' => ['id' => 'selected']
    ];


    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 998,
      'search' => [
        '#type' => 'submit',
        '#value' => 'Search',
        '#name' => 'search'
      ],
      'replace' => [
        '#type' => 'submit',
        '#value' => 'Replace',
        '#name' => 'replace'
      ]
    ];



    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $selected = array_unique(explode('|', $form_state->getValue('selected')));
    $search = $form_state->getValue('search_text');
    $replace = $form_state->getValue('replace_text');
    $entityFields = SNRController::entityFields();
    $operation = new SNROperation($search, $replace); // TODO options
    $batch = [
      'operations' => [],
    ];


    $button = $form_state->getTriggeringElement();
    if ($button['#name'] === 'search') {
      $batch['title'] = t('Searching for @search', ['@search' => $search]);
      $batch['finished'] = ['\Drupal\snr\SNRController', 'searchBatchFinished'];
      $batchOperation = 'searchBatchOperation';
    } else if ($button['#name'] === 'replace') {
      $batch['title'] = t('Replacing @search with @replace', ['@search' => $search, '@replace' => $replace]);
      $batch['finished'] = ['\Drupal\snr\SNRController', 'replaceBatchFinished'];
      $batchOperation = 'replaceBatchOperation';
    }

    $table_column_map = [];

    // Columns can be shared across bundles, but we want to expose bundles for filtering
    foreach ($selected as $column_path) {
      list($entity_type_id, $bundle_key, $field_slug, $column) = explode('++', $column_path);
      $field = $entityFields[$entity_type_id]->bundles[$bundle_key]->fields[$field_slug];
      $table = $field->table;
      $table_column_map[] = "{$table}++{$column}";
      if ($field->revision_table) {
        $table_column_map[] = "{$field->revision_table}++{$column}";
      }
    }

//    return drupal_set_message(print_r($table_column_map, true));

    $table_column_map = array_unique($table_column_map);

    foreach ($table_column_map as $table_column_pair) {
      list($table, $column) = explode('++', $table_column_pair);
      $batch['operations'][] = [
        ['\Drupal\snr\SNRController', $batchOperation],
        [$operation, new SNRTarget($table, $column)]
      ];
    }

    batch_set($batch);

  }

}
