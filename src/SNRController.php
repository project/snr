<?php

namespace Drupal\snr;

use Drupal\Core\Render\Markup;

class SNRController {

  public static function searchBatchOperation(SNROperation $operation, $target, &$context): void {
    $reults = 0;
    try {
      $results = \Drupal::database()->query(
        "SELECT count(`{$target->column}`)
      FROM `{$target->table}`
      WHERE `{$target->column}` LIKE '%{$operation->search}%'"
      )->fetch(\PDO::FETCH_COLUMN);
    } catch (\Throwable $e) {

    }
    if ($results > 0) {
      $context['results'][] = [$target, $results];
    }
  }

  public static function replaceBatchOperation(SNROperation $operation, $target, &$context): void {
    $results = 0;
    try {
      $results = \Drupal::database()
        ->update($target->table)
        ->where("`{$target->column}` LIKE '%{$operation->search}%'")
        ->expression($target->column, "REPLACE(`{$target->column}`, '{$operation->search}', '{$operation->replace}')")
        ->execute();
    } catch (\Throwable $e) {

    }

    if ($results > 0) {
      $context['results'][] = [$target, $results];
    }
  }

  public static function searchBatchFinished($success, $results, $operations) {

    $table = [
      '#type' => 'table',
      '#header' => [
        'table' => 'Table',
        'column' => 'Column',
        'found' => 'Found'
      ],
      '#rows' => [],
      '#empty' => 'No results found'
    ];

    foreach ($results as $i => $result) {
      $row = [
        'table' => $result[0]->table,
        'column' => $result[0]->column,
        'found' => $result[1]
      ];
      $table['#rows'][] = $row;
    }

    /** @var \Drupal\Core\Render\Renderer $renderer */
    $renderer = \Drupal::service('renderer');
    $markup = $renderer->render($table);

    \Drupal::messenger()->addMessage($markup);
  }

  public static function replaceBatchFinished($success, $results, $operations) {
    return static::searchBatchFinished($success, $results, $operations);
  }

//  public static function getEntityTableColumns(): array {
//    $operations = [];
//
//    $column_map = static::getColumnMap();
//
//    // Can we look this up?
//    foreach (static::getFieldTypes() as $field_type) {
//
//      $map = \Drupal::service('entity_field.manager')->getFieldMapByFieldType($field_type);
//      foreach ($map as $entity => $fields) {
//        foreach ($fields as $field => $data) {
//          $table = "{$entity}__{$field}";
//          // Note - some fields are attached to their entity - like description on taxonomy
//          $table_exists = \Drupal::database()
//            ->query('SHOW TABLES LIKE :table', [':table' => $table])
//            ->fetchAll(\PDO::FETCH_COLUMN);
//          if (!$table_exists) {
//            continue;
//          }
//          $columns = isset($column_map[$field_type]) ? $column_map[$field_type] : ['value'];
//          foreach ($columns as $column) {
//            $table_column = "{$field}_{$column}";
//            $operations[$table][] = $table_column;
//          }
//        }
//      }
//    }
//
//    return $operations;
//  }

  /**
   * @return \Drupal\snr\SNREntityFieldMap[]
   */
  public static function entityFields(): array {
    $interesting_types = static::getFieldTypes();
    $return = [];

    /** @var \Drupal\Core\Entity\EntityTypeManager $manager */
    $manager = \Drupal::service('entity_type.manager');
    $entity_types = $manager->getDefinitions();

    /** @var \Drupal\Core\Entity\EntityType $entity_type */
    foreach ($entity_types as $entity_type_id => $entity_type) {
      $entityFieldMap = new SNREntityFieldMap();
      $storage = $manager->getStorage($entity_type_id);
      if (!method_exists($storage, 'getTableMapping')) continue;
      /** @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage $mapping */
      $mapping = $storage->getTableMapping();
      $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type_id);
      $base_columns = $mapping->getAllColumns($mapping->getDataTable());

      foreach ($bundles as $bundle_key => $bundle) {
        $bundleMap = new SNRBundleFieldMap();
        $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type_id, $bundle_key);

        /** @var \Drupal\Core\Field\FieldDefinitionInterface $def */
        foreach ($field_definitions as $slug => $def) {
          if (strpos($slug, 'revision') !== false) continue;
          $type = $def->getType();
          $field_storage = $def->getFieldStorageDefinition();

          try {
            $fieldMap = new SNRFieldMap();
            $base_cols = empty($base_columns) ? $mapping->getAllColumns($mapping->getFieldTableName($slug)) : $base_columns;

            $fieldMap->type = $type;
            $fieldMap->table = $mapping->getFieldTableName($slug);
            $fieldMap->columns = $mapping->getColumnNames($slug);
            if (in_array(current(array_values($fieldMap->columns)), $base_cols)) {
              $fieldMap->revision_table = $mapping->getRevisionDataTable();
            } else {
              $fieldMap->revision_table = $mapping->getDedicatedRevisionTableName($field_storage);
            }

          } catch (\Throwable $e) {

          }
           if ($fieldMap) {
             $bundleMap->fields[$slug] = $fieldMap;
           }

        }
        $entityFieldMap->bundles[$bundle_key] = $bundleMap;
      }
      $return[$entity_type_id] = $entityFieldMap;
    }

    return $return;
  }

  public static function getColumnMap(): array {
    // FIXME get these "value" fields programatically
    $column_map = [
      'link' => ['uri'],
      'email' => ['email'],
      'address' => ['address_line1', 'locality'],
    ];

    return $column_map;
  }

  public static function getFieldTypes(): array {
    // FIXME get these columns programatically
    $field_types = [
      'text',
      'text_with_summary',
      'text_long',
      'string_long',
      'string',
      'link',
      'email',
      'address'
    ];
    return $field_types;
  }
}
